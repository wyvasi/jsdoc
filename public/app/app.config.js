'use strict';
angular
.module('EAUTO')
.constant('API_URL',window.location.origin)
.config(function($locationProvider) {
})

.value('$routerRootComponent', 'app')

.component('app', {
  template:'<ng-outlet></ng-outlet>',
  $routeConfig: [
    {path: '/', name: 'HomeComponent', component: 'home', useAsDefault: true},
    {path: '/add', name: 'AddCar', component: 'add' },
    {path: '/edit/:id', name: 'UpdateCar', component: 'update' },
    {path: '/search', name: 'FindCar', component: 'search' },
    {path: '/stats', name: 'StatisticsCar', component: 'stats' },
    
    //{path: '/', name: 'Home' , redirectTo:''}
  ]
})
.component('header',{
	templateUrl:'app/views/partials/header.html'
})

.component('home',{
	templateUrl:'app/views/main.html',
  controller:function(Courses){
    var scope = this;
    scope.courses = [];

    Courses.getCourses(function(data){
      scope.courses = data.data;
    });

    scope.user = {
      name:"",
      prename:"",
      birthday:"",
      nationality:"",
      sex: "M",
      semester:"Winter",
      study_cycle:"Bachelor EQF level 6",
      field_of_education:"Computer Use",
      before_component:[]
    };


    scope.saveDocument = function(){
      var data = scope.user;
      data.courses =[];
      for(var x = 0 ; x< scope.courses.length;x++){
        if( scope.courses[x].checked){
          data.courses.push(scope.courses[x]);
        }
      }
      Courses.generateLink(data,function(link){
        setTimeout("window.open('" + link.directory + "');", 1000);
      });
    }
    scope.updateCourse = function(e){
      console.log(e);
    }

  }
})