angular
  .module('EAUTO')
.component('stats',{
	templateUrl:'/app/views/stats.html',
	controller:function(Car){
		var scope= this;
		// scope.car = {categorie:'A'};

		// scope.save = function(){
		// 	Car.addCar(scope.car,function(data){
		// 		swal('Adaugare auto',data.message);
		// 	})
		// }
		scope.$onInit = function(){
			Car.getYears(function(data){
				console.log(data,data.car.length);
				var MONTHS = [];
				var data2 = [];
				for(var x = 0 ; x< data.car.length;x++){
					MONTHS.push(data.car[x].year);
					data2.push(data.car[x].count);
				}
        var randomColorFactor = function() {
            return Math.round(Math.random() * 255);
        };
        var randomColor = function() {
            return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',.7)';
        };
        var barChartData = {
            labels: MONTHS,
            datasets: [{
                label: 'Numarul de autovehicule inregistrate.',
                backgroundColor: randomColor(),
                data:data2
            }]
        };
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each bar to be 2px wide and green
                elements: {
                    rectangle: {
                        borderWidth: 2,
                        borderColor: randomColor(),
                        borderSkipped: 'bottom'
                    }
                },
                responsive: true,
                barValueSpacing:2,
                legend: {
                    position: 'top',
                },
                scales: {
        yAxes: [{
            display: true,
            ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                // OR //
                beginAtZero: true   // minimum value will be 0.
            }
        }]
    }
            }
        });


			})
		}
	}
})
