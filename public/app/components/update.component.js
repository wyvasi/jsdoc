angular
  .module('EAUTO')
.component('update',{
	templateUrl:'/app/views/add.html',
	controller:function(Car){
		var scope = this;
		scope.car = {};
		scope.revision = {detalii:""};

		scope.$routerOnActivate = function(next) {
	    // Get the hero identified by the route parameter
	    Car.getCar(next.params.id,function(data){
				if(data.success){
					scope.car = data.car;
					scope.car.data_inmatriculare = new Date(data.car.data_inmatriculare);
				}else{
					alert(data.message);
				}
			})
	  };

	  scope.save = function(){
	  	Car.update(scope.car, function(data){
	  		swal(data.message);
	  	});
	  }
	  scope.deleteCar = function(){
	  	Car.deleteCar(scope.car,function(data){
	  		swal(data.message);
	  	})
	  }
	  scope.addRevision = function(){
	  	Car.addRevision({id:scope.car.id,detalii:scope.revision.detalii},function(data){
	  		swal(data.message);
	  		if(data.success){
	  			scope.car.Revisions.push(data.revision);
	  		}
	  	});
	  }
		scope.$onInit = function(){
			// Car.getCar($routeParams.id,function(data){
			// 	if(data.success){
			// 		scope.car = data.car;
			// 	}else{
			// 		alert(data.message);
			// 	}
			// })
		}

	}
})
