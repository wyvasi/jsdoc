'use strict';

angular.module('EAUTO')
.service('Car', function(API_URL,$http,$window) {
  var Car = {};
  

  Car.getCars = function(callback){
    $http.get(API_URL + '/api/getCars').success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    });
  }
  Car.getCar = function(id , callback){
    $http.get(API_URL+'/api/getCar/'+id).success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    });
  }
  Car.addCar = function(car, callback){
    $http.post(API_URL+'/api/addCar',JSON.stringify(car)).success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    }); 
  }
  Car.update = function(car, callback){
    $http.post(API_URL+'/api/updateCar',JSON.stringify(car)).success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    }); 
  }
  Car.addRevision = function(data, callback){
    $http.post(API_URL+'/api/addRevision',JSON.stringify(data)).success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    }); 
  }
  Car.deleteCar = function( data, callback){
     $http.post(API_URL+'/api/deleteCar/'+ data.id).success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    }); 
  }

  Car.getYears = function(callback){
     $http.get(API_URL+'/api/selectAllYears').success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    }); 
  }
  // Car.getCar = 

  return Car;
});
