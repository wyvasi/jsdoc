'use strict';

angular.module('EAUTO')
.service('Courses', function(API_URL,$http,$window) {
  var Courses = {};
  

  Courses.getCourses = function(callback){
    $http.get(API_URL + '/courses2.json').success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    });
  }
  Courses.generateLink = function(data,callback){
    $http.post(API_URL + '/api/createDocument', data).success(function(res){
      return callback(res);
    }).error(function(){
      console.log('There was an error!'); 
    });
  }

  // Car.getCar = 

  return Courses;
});
