'use strict';
var async = require('async');
var Promisify = require('bluebird');
var request = require('request');
var _ = require('lodash');
var fs = require('fs');
var	Docxtemplater = require('docxtemplater');
var moment = require('moment');

module.exports = function(app) {
//	app.get('/api/getUserProfile', ensureAuthenticated, getUserProfile);
	app.post('/api/createDocument',function(req , res){

		var content = fs.readFileSync("./la.docx","binary");
		var doc=new Docxtemplater(content);
		console.log(req.body)
	  var total =0;
	  for(var x = 0;x < req.body.courses.length;x++){
	  	total += parseInt(req.body.courses[x].ects);
	  }
	  var body = req.body;
	  body.total = total;
	  doc.setData(body);
		try{
			doc.render();
		}catch(e){
			console.log(e)
			return res.status(200).json({success:false,message:"There was an error processing your data!",error:e});
		}
		var buf = doc.getZip().generate({type:"nodebuffer"});
		var name = "";
		if(req.body.name && req.body.prename){
			name = req.body.name + "_" + req.body.prename + ".docx";
			fs.writeFileSync("./public/documents/" + name,buf);
		}else{
			name = moment().unix() + ".docx";
			fs.writeFileSync("./public/documents/" +name,buf);
		}

		return res.status(200).json({success:true,directory:'/documents/'+name});
	});

	app.get('/api/convertData', function(req ,res ){
		var string = fs.readFileSync('data.txt','utf-8');
		string = string.split('\r\n');
		var data = [];
		var index = 0;
		for(var x = 0 ; x< string.length -1;x++){
			let dat = string[x].split(' ');
			let dd = dat.slice(0,3);
			var text = "";
			for(var y = 3; y< dat.length;y++){
				if(isNaN(dat[y])){
					text += dat[y]+" ";
					index = y;
				}else{
					break;
				}
			}
			dd.push(text);

		//	console.log(index);
			dd.push(dat[index+1]);
			text = "";
			//console.log(dat[index+1])
			for(var y = index+2; y< dat.length;y++){
				if(isNaN(dat[y])){
					text += dat[y]+" ";
					//index = y;
				}else{
					index = y;
					break;
				}
			}
			//console.log(text);

			dd.push(text);
			dd =dd.concat(dat.slice(index+1,index + 2));
			var reverse = dat.reverse();
			reverse = reverse.slice(0,2).reverse();
			dd = dd.concat(reverse);
			// for(var y = index+1; y< dat.length;y++){
			// 	if(isNaN(dat[y])){
			// 		dd += " "+dat[y];
			// 		//index = y;
			// 	}else{
			// 		break;
			// 	}
			// }

			console.log(dd);

			data.push(dd);
		}
		console.log(data);
		var objects=[];
		for(var x = 0; x< data.length ; x++){
			objects.push({
				field_of_study:data[x][3],
				name: data[x][5],
				ects: data[x][6],
				semester:data[x][8],
				course_code:data[x][0],
				language:data[x][1],
				faculty:data[x][2],
				examination:data[x][7],
				checked:false
			})
		}
		fs.writeFileSync('./public/courses2.json',JSON.stringify({succcess:true,data:objects}, null, ' '))
		return res.status(200).json({success:true,message:objects});
	})
}

      // "field_of_study":"Software and applications development and analysis",
      // "name": "Artificial intelligence in control applications",
      // "ects":5,
      // "semester":"winter",
      // "course_code":"61E40",
      // "language":"EN",
      // "faculty": "FMECS",
      // "examination":"A",
      // "checked":false