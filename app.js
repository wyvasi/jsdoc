'use strict';

var express = require('express');
var app = express();
//var Sequelize = require('sequelize');
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));  
app.use(express.static(__dirname + '/public'));

//var models = require("./models");


require('./services/api.js')(app);


app.listen(80,function(){
	console.log('Started on port 80!');
})


// var fs=require('fs');
// Docxtemplater = require('docxtemplater');

// content = fs
//     .readFileSync(__dirname+"/la.docx","binary");

// doc=new Docxtemplater(content);
// console.log(doc);


// doc.setData({
//     "firstname":"John",
//     "lastname":"Doe"
// });
// console.log("set data");

// try{
// 	doc.render();
// }catch(e){
// 	console.log(e);
// }
// console.log("render");
// var buf = doc.getZip()
//              .generate({type:"nodebuffer"});
// console.log("generate");
// fs.writeFileSync(__dirname+"/output.docx",buf);