61D00 EN FEE Computer use 0611 Business Law and Protection of Intellectual Property 1 2 15 15 A S
61D01 EN FEE Computer use 0611 Concurrent Processing 1 3 15 15 A S
61D02 EN FEE Computer use 0611 Cryptography 2 5 30 30 EW S
61D03 EN FEE Computer use 0611 Embedded Systems (Faculty of Electrical Engineering) 1 3 15 30 A S
61D04 EN FEE Computer use 0611 Fundamentals of Organization and Management 1 3 15 15 A S
61D05 EN FEE Computer use 0611 Operational research 2 4 30 15 A S
61D06 EN FEE Computer use 0611 Probabilistic Methods and Statistics 1 4 30 15 EW S
61E20 EN FMECS Database and network design and administration 0612 Database Server Programming and Administration 2 3 15 15 A S
61E21 EN FMECS Database and network design and administration 0612 LAN and WAN networks 2 4 30 EW S
61E22 EN FMECS Database and network design and administration 0612 Modelling of heat and fluid flow processes 2 6 45 45 EW S
61E40 EN FMECS Software and applications development and analysis 0613 Artificial intelligence in control applications 2 5 30 15 30 A S
61E41 EN FMECS Software and applications development and analysis 0613 Asynchronous WWW interfaces 2 3 15 15 A S
61E42 EN FMECS Software and applications development and analysis 0613 Big data& data mining 2 5 30 30 1 A S
61E43 EN FMECS Software and applications development and analysis 0613 Concurrent and Distributed Programming 1 6 30 30 EW S
61E44 EN FMECS Software and applications development and analysis 0613 Fundamentals of programming (C++) 1 5 30 30 EW S
61E45 EN FMECS Software and applications development and analysis 0613 Inteligent systems of signal processing 2 5 30 30 EW S
61E46 EN FMECS Software and applications development and analysis 0613 Medical imaging techniques 1 5 30 30 EW S
61E47 EN FMECS Software and applications development and analysis 0613 Neural networks & machine learning 2 5 30 15 30 A S
61E48 EN FMECS Software and applications development and analysis 0613 Numerical methods 1 5 30 30 EW S
61E49 EN FMECS Software and applications development and analysis 0613 Object programming 1 4 30 30 A S
61E50 EN FMECS Software and applications development and analysis 0613 Programming of interactive computer graphics on WWW 3 6 30 30 EW S
61E51 EN FMECS Software and applications development and analysis 0613 Theory of games and decisions 2 5 30 30 15 EW S
